'use strict'

import * as Hapi from '@hapi/hapi'
import * as HapiSwagger from 'hapi-swagger'
import * as Inert from '@hapi/inert'
import * as Vision from '@hapi/vision'

import { Server } from '@hapi/hapi'
import { routes } from './routes'
import { Bank } from './Bank'

export let server: Hapi.Server

export const init = async function (bankApplication: Bank): Promise<Server> {
  server = Hapi.server({
    port: process.env.PORT || 4000,
    host: '0.0.0.0'
  })

  const swaggerOptions: HapiSwagger.RegisterOptions = {
    info: {
      title: 'BankRepository Kata API Documentation',
      description: '<h2>Instructions On How to use the Swagger API :</h2><hr>The Bank Kata is an In Memory Application so you can only use the following account numbers :<b><ul><li>Joe Bidjoba: FR7630006000011234567890189</li><br><li>Sigurd Joensen: FO9264600123456789</li><br><li>Bob Rich: GB33BUKB20201555555555</li></ul></b><hr>',
      version: '0.1'
    }
  }

  const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
    {
      plugin: Inert
    },
    {
      plugin: Vision
    },
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]

  await server.register(plugins)

  server.route(routes(bankApplication))

  return server
}

export const start = async function (): Promise<void> {
  try {
    await server.start()
    console.log('Server running at:', server.info.uri)
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
}

process.on('unhandledRejection', (err) => {
  console.error('unhandledRejection')
  console.error(err)
  process.exit(1)
})
